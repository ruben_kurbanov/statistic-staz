import java.util.Calendar;

public class Record{

    private String firstCamera;
    private String secondCamera;
    private Calendar firstTime;
    private Calendar secondTime;
    private int countCyclist;

    public Record(String firstCamera, String secondCamera, Calendar firstTime, Calendar secondTime, int countCyclist){
        this.firstCamera = firstCamera;
        this.secondCamera = secondCamera;
        this.firstTime = firstTime;
        this.secondTime = secondTime;
        this.countCyclist = countCyclist;
    }


    public void setFirstCamera(String firstCamera) {
        this.firstCamera = firstCamera;
    }

    public void setSecondCamera(String secondCamera) {
        this.secondCamera = secondCamera;
    }

    public void setFirstTime(Calendar firstTime) {
        this.firstTime = firstTime;
    }

    public void setSecondTime(Calendar secondTime) {
        this.secondTime = secondTime;
    }

    public void setCountCyclist(int countCyclist) {
        this.countCyclist = countCyclist;
    }


    public String getFirstCamera() {
        return firstCamera;
    }

    public String getSecondCamera() {
        return secondCamera;
    }

    public Calendar getFirstTime() {
        return firstTime;
    }

    public Calendar getSecondTime() {
        return secondTime;
    }

    public int getCountCyclist() {
        return countCyclist;
    }

    @Override
    public String toString() {
        return "Record{" +
                "firstCamera='" + firstCamera + '\'' +
                ", secondCamera='" + secondCamera + '\'' +
                ", firstTime=" + firstTime +
                ", secondTime=" + secondTime +
                ", countCyclist=" + countCyclist +
                '}';
    }
}

import java.util.ArrayList;
import java.util.List;

public class Parametrs {

    private String source;
    private List<Statistics> statistics = new ArrayList<>();
    private List<OutputFiles> outputFiles = new ArrayList<>();

    public Parametrs(String source, String statistics, String outputFiles){
        this.source = source;
        setStatistics(statistics);
        setOutputFiles(outputFiles);
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public List<Statistics> getStatistics() {
        return this.statistics;
    }

    /**
     * Takes a parameter of the "string" type, splits into an array of
     * strings, fills the "statistics" list with enumerations
     *
     * @param statistics String
     */
    public void setStatistics(String statistics) {

        String[] statisticsToArray = statistics.toUpperCase().split(",");

        for (String statistic : statisticsToArray){
            switch (statistic) {
                case "CP":
                    this.statistics.add(Statistics.CP);
                    break;
                case "PP":
                    this.statistics.add(Statistics.PP);
                    break;
                case "CK":
                    this.statistics.add(Statistics.CK);
                    break;
                case "DM":
                    this.statistics.add(Statistics.DM);
                    break;
                case "NH":
                    this.statistics.add(Statistics.NH);
                    break;
                default:
                    break;
            }
        }
    }

    public List<OutputFiles> getOutputFiles() {
        return this.outputFiles;
    }

    /**
     * Takes a parameter of the "string" type, splits into an array of
     * strings, fills the "outputsFiles" list with enumerations
     *
     * @param outputFiles String
     */
    public void setOutputFiles(String outputFiles) {

        String[] outputsToArray = outputFiles.toUpperCase().split(",");

        for (String outputs: outputsToArray){
            switch (outputs) {
                case "STRING":
                    this.outputFiles.add(OutputFiles.STRING);
                    break;
                case "CSV":
                    this.outputFiles.add(OutputFiles.CSV);
                    break;
                case "XML":
                    this.outputFiles.add(OutputFiles.XML);
                    break;
                case "JSON":
                    this.outputFiles.add(OutputFiles.JSON);
                    break;
                default:
                    break;
            }
        }
    }
}

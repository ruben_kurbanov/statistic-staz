import java.io.BufferedWriter;
import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import au.com.bytecode.opencsv.CSVWriter;

import com.github.cliftonlabs.json_simple.JsonArray;
import com.github.cliftonlabs.json_simple.JsonObject;
import com.github.cliftonlabs.json_simple.Jsoner;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public enum OutputFiles {

    STRING{
        /**
         * Writes statistics results to the console
         *
         * @param results List<Result>
         */
        public void action(List<Result> results) {
            for (Result r: results){
                System.out.println(r.toString());
            }
        }
    },
    CSV{
        /**
         * Creates a file csv_export.csv in directory export and writes all results to it
         *
         * @param results List<Result>
         * @throws IOException
         */
        public void action(List<Result> results) throws IOException {
            String csvOutputFile = "export/csv_export.csv";
            CSVWriter writer = new CSVWriter(new FileWriter(csvOutputFile, true));

            for (Result r: results){
                writer.writeNext(new String[]{r.getDescriptionOfStatistics()});
                for (String s: r.getResults()){
                    writer.writeNext(new String[]{s});
                }
                writer.writeNext(new String[0]);

            }
            writer.close();
        }

    },
    XML{
        /**
         *
         * Creates a file xml_export.xml in directory export and writes all results to it
         *
         * @param results List<Result>
         */
        public void action(List<Result> results) {

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;

            String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());


            try {
                builder = factory.newDocumentBuilder();

                // создаем пустой объект Document, в котором будем
                // создавать наш xml-файл
                Document doc = builder.newDocument();
                // создаем корневой элемент
                Element report =
                        doc.createElement("report");

                Element statisticsResults =
                        doc.createElement("StatisticsResults");

                for (Result r: results){
                    statisticsResults.appendChild(getResult(doc, r.getDescriptionOfStatistics(), r.getResults()));
                }

                //set the result creation date
                report.appendChild(getResultElements(doc, "created", timeStamp));
                report.appendChild(statisticsResults);

                doc.appendChild(report);


                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();

                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                DOMSource source = new DOMSource(doc);

                StreamResult file = new StreamResult(new File("export/xml_export.xml"));

                //записываем данные
                transformer.transform(source, file);
                //System.out.println("Создание XML файла закончено");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /**
         *
         * @param doc Document
         * @param name String
         * @param results List<String>
         * @return Node
         */
        private Node getResult(Document doc, String name, List<String> results) {
            Element statistics = doc.createElement("statistics");

            // создаем элемент name
            statistics.appendChild(getResultElements(doc,"name", name));

            for (String result: results){
                statistics.appendChild(getResultElements(doc, "result", result));
            }

            return statistics;
        }


        /**
         * utility method for creating a new XML file node
         *
         * @param doc Document
         * @param name String
         * @param value String
         * @return Node
         */
        private Node getResultElements(Document doc, String name, String value) {
            Element node = doc.createElement(name);
            node.appendChild(doc.createTextNode(value));
            return node;
        }

    },
    JSON{
        /**
         * Creates a file json_export.json in directory export and writes all results to it
         * @param results List<Result>
         */
        public void action(List<Result> results) {

            String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());

            try {

                // create a writer
                BufferedWriter writer = Files.newBufferedWriter(Paths.get("export/json_export.json"));

                // create customer object
                JsonObject root = new JsonObject();
                root.put("created", timeStamp);

                JsonArray res = new JsonArray();

                for (Result r: results){
                    JsonObject result = new JsonObject();

                    for (int i = 0; i < r.getResults().size(); i++)
                        result.put("result", r.getResults());

                    result.put("name", r.getDescriptionOfStatistics());

                    res.add(result);
                }

                root.put("statistics", res);

                // write JSON to file
                Jsoner.serialize(root, writer);

                //close the writer
                writer.close();

            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    };

    public abstract void action(List<Result> results) throws IOException;


}

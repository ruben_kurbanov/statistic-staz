import au.com.bytecode.opencsv.CSVReader;

import java.io.FileReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main extends Parametrs{

    public Main(String source){
        super(source, "", "");
    }

    public Main(String source, String statistics){
        super(source, statistics, "");
    }

    public Main(String source, String statistics, String outputFiles){
        super(source, statistics, outputFiles);
    }

    /**
     * Requests a list of statistics in the console if it has not been previously declared.
     */
    private void statisticsRequest(){

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the list(or only one) of statistics you are interested in\n" +
                "Statistics: \n" +
                    "CP - The total number of cyclists recorded;\n" +
                    "PP - The average number of recorded cyclist passes per day;\n" +
                    "CK - The total number of recorded cyclist passes for each day;\n" +
                    "DM - Day with the maximum number of records;\n" +
                    "NH - The most popular hour during the day for cycling;\n");

        this.setStatistics(sc.nextLine());
    }

    /**
     * Requests a list of output files in the console if it has not been previously declared.
     */
    private void outputFilesRequest(){

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a list (or only one) of the response output formats\n" +
                "Output formats: \n" +
                        "• STRING - simple string to console;\n" +
                        "• CSV;\n" +
                        "• XML;\n" +
                        "• JSON;\n");

        this.setOutputFiles(sc.nextLine());
    }

    public void getResult() throws Exception {

        List<RecordingsOfOneDay> allDays = setData();

        List<Result> results = new ArrayList<>();

        if (this.getStatistics().size() == 0) statisticsRequest();
        if (this.getOutputFiles().size() == 0) outputFilesRequest();

        for (Statistics statistic: this.getStatistics()){

            switch (statistic) {
                case CP:
                    List<String> resultCP = Statistics.CP.action(allDays);
                    String descriptionCP = "The total number of cyclists recorded";
                    results.add(new Result(descriptionCP, resultCP));
                    break;
                case PP:
                    List<String> resultPP = Statistics.PP.action(allDays);
                    String descriptionPP = "The average number of recorded cyclist passes per day";
                    results.add(new Result(descriptionPP, resultPP));
                    break;
                case CK:
                    List<String> resultCK = Statistics.CK.action(allDays);
                    String descriptionCK = "The total number of recorded cyclist passes for each day";
                    results.add(new Result(descriptionCK, resultCK));
                    break;
                case DM:
                    List<String> resultDM = Statistics.DM.action(allDays);
                    String descriptionDM = "Day with the maximum number of records";
                    results.add(new Result(descriptionDM, resultDM));
                    break;
                case NH:
                    List<String> resultNH = Statistics.NH.action(allDays);
                    String descriptionNH = "The most popular hour during the day for cycling";
                    results.add(new Result(descriptionNH, resultNH));
                    break;
                default:
                    break;
            }

        }

        for (OutputFiles outputs: this.getOutputFiles()){
            outputs.action(results);
        }

    }


    private Calendar[] stringToCalendar(String from, String to) throws ParseException {

        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

        Calendar measuredFrom = Calendar.getInstance();
        Calendar measuredTo = Calendar.getInstance();


        measuredFrom.setTime(inputFormat.parse(from));
        measuredTo.setTime(inputFormat.parse(to));

        return new Calendar[]{measuredFrom, measuredTo};
    }

    private boolean nextDay(Calendar measuredFrom, Calendar actualDate){

        return (measuredFrom.get(Calendar.DATE) != actualDate.get(Calendar.DATE) ||
                measuredFrom.get(Calendar.MONTH) != actualDate.get(Calendar.MONTH) ||
                measuredFrom.get(Calendar.YEAR) != actualDate.get(Calendar.YEAR));
    }

    @SuppressWarnings("resource")
    private List<RecordingsOfOneDay> setData() throws Exception{

        CSVReader reader = new CSVReader(new FileReader(this.getSource()), ',' , '"' , 1);
        String[] nextLine;

        List<Record> recordings = new ArrayList<Record>();
        List<RecordingsOfOneDay> allDays = new ArrayList<RecordingsOfOneDay>();
        int totalCyclistsOfOneDay = 0;
        Calendar actualDate = new GregorianCalendar(1000, Calendar.JANUARY , 1);

        while ((nextLine = reader.readNext()) != null) {

            if (nextLine[4].equals("") || Integer.parseInt(nextLine[4]) == 0)
                continue;

            Calendar[] interval = stringToCalendar(nextLine[2], nextLine[3]);

            if (actualDate.get(Calendar.YEAR) == 1000){
                actualDate.setTime(interval[0].getTime());

            }

            if(nextDay(interval[0], actualDate)){

                Calendar newCalendar = new GregorianCalendar(actualDate.get(Calendar.YEAR),
                        actualDate.get(Calendar.MONTH),
                        actualDate.get(Calendar.DATE));

                allDays.add(new RecordingsOfOneDay(recordings, newCalendar, totalCyclistsOfOneDay));

                actualDate.setTime(interval[0].getTime());

                totalCyclistsOfOneDay = 0;

                recordings.clear();

            }

            totalCyclistsOfOneDay += Integer.parseInt(nextLine[4]);
            recordings.add(new Record(nextLine[0], nextLine[1], interval[0], interval[1], Integer.parseInt(nextLine[4])));

        }

        allDays.add(new RecordingsOfOneDay(recordings, actualDate, totalCyclistsOfOneDay));

        return allDays;
    }


}

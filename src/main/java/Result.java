import java.util.List;

public class Result {

    private String descriptionOfStatistics;

    private List<String> results;

    public Result(String descriptionOfStatistics, List<String> results){
        this.descriptionOfStatistics = descriptionOfStatistics;
        this.results = results;
    }

    public String getDescriptionOfStatistics() {
        return descriptionOfStatistics;
    }

    public void setDescriptionOfStatistics(String descriptionOfStatistics) {
        this.descriptionOfStatistics = descriptionOfStatistics;
    }

    public List<String> getResults() {
        return results;
    }

    public void setResults(List<String> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "Result{" +
                "name='" + descriptionOfStatistics + '\'' +
                ", results=" + results.toString() +
                '}';
    }
}

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

enum Statistics {

    CP{ //celkový počet zaznamenaných průjezdů cyklistů

        /**
         *Goes through the list of all days with camera recordings and adds up the number of cyclists.
         *Each RecordingsOfOneDay class already has a counted number of cyclists.
         *
         * @param allDays List<RecordingsOfOneDay>
         * @return List<String>
         */
        public List<String> action(List<RecordingsOfOneDay> allDays){
            int total = 0;
            for (RecordingsOfOneDay r : allDays){
                total += r.getNumberOfCyclists();
            }

            List<String> result = new ArrayList<String>();
            result.add(String.valueOf(total));

            return new ArrayList<String>(result);
        }
    },

    PP{ //průměrný počet zaznamenaných průjezdů cyklistů za den

        /**
         *It goes through the list of all days in which there are recordings from cameras,
         *  and adds up the number of cyclists, and then divides by the length of the list (number of days).
         *  Each RecordingsOfOneDay class already has a counted number of cyclists.
         *
         * @param allDays List<RecordingsOfOneDay>
         * @return List<String>
         */
        public List<String> action(List<RecordingsOfOneDay> allDays){

            int total = 0;

            for (RecordingsOfOneDay rec : allDays){
                total += rec.getNumberOfCyclists();
            }

            List<String> result = new ArrayList<String>();
            result.add(String.valueOf(total/allDays.size()));

            return new ArrayList<String>(result);
        }
    },
    CK{ //celkový počet zaznamenaných průjezdů cyklistů pro každý den

        /**
         *Goes through the list of all days with camera recordings,
         *  creates a list of the number of recorded cyclists for each day.
         *  Each RecordingsOfOneDay class already has a counted number of cyclists.
         *
         * @param allDays List<RecordingsOfOneDay>
         * @return List<String>
         */
        public List<String> action(List<RecordingsOfOneDay> allDays){

            int total = 0;

            List<String> result = new ArrayList<String>();

            for (RecordingsOfOneDay rec : allDays){
                String timeStamp = new SimpleDateFormat("yyyy-MM-dd: ").format(rec.getDate().getTime());
                result.add(timeStamp + String.valueOf(rec.getNumberOfCyclists()));
            }

            return new ArrayList<String>(result);

        }
    },
    DM{ // den s maximálním počtem záznamů

        /**
         *
         * It goes through the list of all days with camera recordings
         *  and determines the day with the maximum number of recorded cyclists.
         * Each RecordingsOfOneDay class already has a counted number of cyclists.
         *
         * @param allDays List<RecordingsOfOneDay>
         * @return List<String>
         */
        public List<String> action(List<RecordingsOfOneDay> allDays){

            RecordingsOfOneDay max = allDays.get(0);

            for (RecordingsOfOneDay record : allDays){
                if (record.getNumberOfCyclists() > max.getNumberOfCyclists()) max = record;
            }

            List<String> result = new ArrayList<String>();
            String timeStamp = new SimpleDateFormat("yyyy-MM-dd: ").format(max.getDate().getTime());
            result.add(timeStamp + String.valueOf(max.getNumberOfCyclists()));

            return new ArrayList<String>(result);
        }
    },


    NH{ //nejoblíbenější hodina v průběhu dne pro jízdu na kole;

        /**
         * It goes through the list of all days in which there are camera recordings,
         * determines the best time for each day,
         * by searching for the maximum value of recorded cyclists every hour during the day.
         *
         * @param allDays List<RecordingsOfOneDay>
         * @return List<String>
         */
        public List<String> action(List<RecordingsOfOneDay> allDays){
            int[] days = new int[24];

            for (RecordingsOfOneDay record : allDays){

                Record max = record.getRecords().get(0);

                for (Record r : record.getRecords()){
                    if (r.getCountCyclist() > max.getCountCyclist()) max = r;

                }

                days[max.getFirstTime().get(Calendar.HOUR_OF_DAY)]++;
            }

            int betterHour = 0;

            for (int i = 0; i < days.length; i++){
                if (days[i] > days[betterHour]) betterHour = i;
            }

            List<String> result = new ArrayList<String>();
            result.add(String.valueOf(betterHour));

            return new ArrayList<String>(result);
        }
    };

    public abstract List<String> action(List<RecordingsOfOneDay> allDays);

}

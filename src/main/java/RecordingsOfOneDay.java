
import java.util.Calendar;
import java.util.List;

public class RecordingsOfOneDay{

    private List<Record> records;
    private Calendar date;
    private int numberOfCyclists;

    public RecordingsOfOneDay(List<Record> records, Calendar date, int numberOfCyclists){
        this.records = records;
        this.date = date;
        this.numberOfCyclists = numberOfCyclists;

    }

    public List<Record> getRecords() {
        return records;
    }

    public void setRecords(List<Record> records) {
        this.records = records;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public int getNumberOfCyclists() {
        return numberOfCyclists;
    }

    public void setNumberOfCyclists(int numberOfCyclists) {
        this.numberOfCyclists = numberOfCyclists;
    }

    @Override
    public String toString() {
        return "RecordingsOfOneDay{" +
                "records=" + records +
                ", date=" + date +
                ", numberOfCyclists=" + numberOfCyclists +
                '}';
    }
}

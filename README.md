# Statistic


Pro použití programy máte vytvořit instance třídy Main.

Máte 3 možnosti:

1) new Main("import/fileName.csv") - Na Statistiky a spůsoby exportů se program zeptá pozdějí
2) new Main("import/fileName.csv", "cp,ck,nh") - Na spůsoby exportů se program zeptá pozdějí (Statistiky lze zadávat jak velkými, tak malými písmeny, počet je neomezený NEMEZERUJTE)
3) new Main("import/fileName.csv", "cp,ck,nh", "json, string, csv") (Spůsoby exportů lze zadávat jak velkými, tak malými písmeny, počet je neomezený NEMEZERUJTE)

Chcete-li spustit program, zavolejte metodu main.getResult();

